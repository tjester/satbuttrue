# Lisp

A QBF solver was the first (and only) programming project I've done with the [Lisp programming language](https://en.wikipedia.org/wiki/Lisp_(programming_language)). Today, I'm going back to face my fears and write a SAT solver in Lisp.
