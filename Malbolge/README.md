# Malbolge

[Malbolge](https://web.archive.org/web/20031209180058/www.mines.edu/students/b/bolmstea/malbolge/index.html) is the 8th cricle of hell in Dante's inferno. But it also a crytpographic language making it one of the most difficult prgramming language to code in. That's why we're gonna attempt to create a SAT solver in this beautiful language ! To be more precise, we are gonna create a program that can generate such a code, because it is by designed impossible to code by hand.

The documentation can be found here : [https://web.archive.org/web/20031209180058/www.mines.edu/students/b/bolmstea/malbolge/index.html](https://web.archive.org/web/20031209180058/www.mines.edu/students/b/bolmstea/malbolge/index.html)
