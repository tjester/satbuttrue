# SATbutTrue

Have you ever wonder in how many languages you can code a SAT solver ? <br>
Me neither, I will just do it until I get bored.

With a friend of mine, we will just test as many (often weird) languages and compare them on the exercise of solving satisfiability problems.

Each language has its own folder in which we specifed the work done and more or less important information. In general we will try to implement the DPLL algorithm because naive is boring. However, we might be temped, from time to time, to go up to a CDCL solver.

Hope you enjoy :)
