# Uiua

[Uiua](https://www.uiua.org/) is my first array-oriented programming language and this whole project started deciding that we wanted to redo our sat solver project in Uiua (and [Scratch](/Scratch)).

Same as [lc-3](/lc-3), we will do 2 subprojects: one where we focus on efficiency and the other where we try do be as concise as possible.
