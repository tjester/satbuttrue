# ISO/IEC 9899:1999 (C99)

[C99](https://en.wikipedia.org/wiki/C99) is the most wildly implemented version of C. We want here to build the fastest SAT solver we can using C99.
